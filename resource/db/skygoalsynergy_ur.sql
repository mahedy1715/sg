-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 22, 2019 at 08:26 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skygoalsynergy_ur`
--

-- --------------------------------------------------------

--
-- Table structure for table `cualumni`
--

CREATE TABLE IF NOT EXISTS `cualumni` (
  `id` int(11) NOT NULL,
  `role` varchar(5) NOT NULL DEFAULT 'User',
  `full_name` varchar(100) DEFAULT NULL,
  `membertype` varchar(2) NOT NULL DEFAULT '2',
  `photo` varchar(30) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `blood` varchar(20) DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `present_address` varchar(250) DEFAULT NULL,
  `permanent_address` varchar(250) DEFAULT NULL,
  `batch` varchar(10) DEFAULT NULL,
  `id_no` varchar(20) DEFAULT NULL,
  `honours` varchar(5) DEFAULT NULL,
  `ma` varchar(5) DEFAULT NULL,
  `mphil` varchar(5) DEFAULT NULL,
  `phd` varchar(5) DEFAULT NULL,
  `mobile_no` varchar(13) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `self` int(4) DEFAULT NULL,
  `spouse` int(4) DEFAULT '0',
  `guest` int(4) DEFAULT '0',
  `child` int(4) DEFAULT '0',
  `infant` int(4) DEFAULT '0',
  `baby` int(4) DEFAULT '0',
  `lmfees` int(11) DEFAULT NULL,
  `total` int(4) DEFAULT NULL,
  `food` varchar(10) DEFAULT NULL,
  `Payment_mode` varchar(10) DEFAULT NULL,
  `payees_name` varchar(50) DEFAULT NULL,
  `transaction_no` varchar(60) DEFAULT NULL,
  `note` varchar(250) DEFAULT NULL,
  `payment_status` varchar(50) NOT NULL DEFAULT 'Unpaid',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `token` varchar(255) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `softdeleted` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cualumni`
--

INSERT INTO `cualumni` (`id`, `role`, `full_name`, `membertype`, `photo`, `dob`, `blood`, `institution`, `designation`, `present_address`, `permanent_address`, `batch`, `id_no`, `honours`, `ma`, `mphil`, `phd`, `mobile_no`, `email`, `self`, `spouse`, `guest`, `child`, `infant`, `baby`, `lmfees`, `total`, `food`, `Payment_mode`, `payees_name`, `transaction_no`, `note`, `payment_status`, `created`, `token`, `modified`, `softdeleted`) VALUES
(1, 'User', 'Mainul Hasan Chowdhury', '2', '88018177950663575.jpg', '1958-01-01', 'B+', 'University of Chittagong', 'Professor & Chair', 'University of Chittagong', 'University of Chittagong', '12th', '', '', '1981', NULL, NULL, '01817795066', 'mhasan1958@yahoo.com', 2000, 1500, NULL, NULL, NULL, NULL, NULL, 3500, 'Non-Beef', 'Cash', 'Mainul Hasan Chowdhury', '01817795066', '01817795066 - Cash', 'Paid', '2018-12-07 13:37:44', NULL, '2018-12-07 13:37:44', 'No'),
(2, 'User', 'Sukanta Bhattacharjee', '2', '88018196374913221.jpg', '2018-03-14', 'O+', 'University of Chittagong', 'Professor ', 'University of Chittagong', 'University of Chittagong', '10th', '01819637491', '', '1988', NULL, NULL, '01819637491', 'sb@cuenglishalumni.org', 2000, 1500, NULL, NULL, NULL, NULL, NULL, 3500, 'Non-Beef', 'Cash', 'Sukanta Bhattacharjee', '01819637491', '01819637491', 'Paid', '2018-12-07 13:55:47', NULL, '2018-12-07 13:55:47', 'No'),
(3, 'Admin', 'Muhammed Rukan Uddin', '2', '019244418783684.jpg', '1980-02-02', 'B+', 'Chittagong University', 'Associate Professor', 'Chittagong University', 'Banskhali Ctg.', '33rd', '000000', '2004', '2005', NULL, NULL, '01924441878', 'rukan.uddin1980@gmail.com ', 2000, 1500, NULL, 1000, 500, NULL, NULL, 5000, 'Beef', 'Cash', 'Rukan', '', 'Cash', 'Unpaid', '2018-12-07 17:18:02', NULL, '2018-12-07 17:18:02', 'No'),
(4, 'User', 'Md.Kamrul hasan ', '2', '016762475193332.jpg', '1991-06-01', 'B+', 'Exim bank', 'Assistant officer  ', 'Feni', 'Feni', '45th', '10102095', '2013', '2014', NULL, NULL, '01676247519', 'Kamrul4766@Gmail. Com ', 2000, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 'Beef', 'Bkash', 'Kamrul hasan', '01676247519', 'Exim bank chhagalnaiya branch  ', 'Unpaid', '2018-12-12 14:12:07', NULL, '2018-12-12 14:12:07', 'No'),
(5, 'User', 'Fatema Zannat', '2', '017276594943986.jpg', '1986-12-31', 'B+', 'National Board of Revenue', 'Assistant Revenue Officer', 'R-1,H-823,Chandrima R/A, Chandgaon, Chittagong', 'R-1,H-823,Chandrima R/A, Chandgaon,Chittagong', '39th', '3117', '2010', '2012', NULL, NULL, '01727659494', 'fatemazannatcu@gmail.com', 2000, 1500, NULL, NULL, NULL, 0, NULL, 3500, 'Beef', 'Rocket', 'Md. Jakir Hussen', '114316493', '01819614810', 'Unpaid', '2018-12-14 18:25:11', NULL, '2018-12-14 18:25:11', 'No'),
(6, 'User', 'S.M Nurul Alam Khan ', '2', '01818707003375.jpg', '1986-09-01', 'AB+', 'National Board of Revenue ', 'Assistant Revenue Officer ', 'Khan Bari, Bairag, Anwara, Chattogram', 'Khan Bari,  Bairag, Anwara,  Chattogram', '41st', '4005', '2009', '2010', NULL, NULL, '01818707003', 'nurulalam847@gmail.com', 2000, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 'Beef', 'Bkash', 'Alamgir', '3RUY', '6AB473RUY', 'Unpaid', '2019-01-11 16:44:48', NULL, '2019-01-11 16:44:48', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `doantion`
--

CREATE TABLE IF NOT EXISTS `doantion` (
  `id` int(11) NOT NULL,
  `donortype` varchar(1) NOT NULL DEFAULT 'M',
  `dmid` int(4) DEFAULT NULL,
  `amount` float(11,2) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `softdeleted` varchar(3) DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE IF NOT EXISTS `donor` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `note` varchar(500) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `softdeleted` varchar(3) DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(15) NOT NULL,
  `senderid` varchar(20) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `username`, `password`, `senderid`, `url`) VALUES
(1, 'sms', 'olineit', 'aiiew100%bir', '8804445650538', 'http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$msg&GSM=$gsm&type=longSMS'),
(2, 'mail', 'olineit', 'aiiew100%bir', '8804445650538', 'http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$msg&GSM=$gsm&type=longSMS');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `role` varchar(5) NOT NULL,
  `permission` varchar(2000) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cualumni`
--
ALTER TABLE `cualumni`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_no` (`id_no`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_no_2` (`mobile_no`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `mobile_no` (`mobile_no`);

--
-- Indexes for table `doantion`
--
ALTER TABLE `doantion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cualumni`
--
ALTER TABLE `cualumni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doantion`
--
ALTER TABLE `doantion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donor`
--
ALTER TABLE `donor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
