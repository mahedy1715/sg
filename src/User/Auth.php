<?php
namespace App\User;
if(!isset($_SESSION) )  session_start();
use App\Model\Database as DB;
use PDO;

class Auth extends DB{

    public $email = "";
    public $password = "";
    public $sessionPeriod=100;
    public $sessionPeriodMultiply=60;
    private $loginas,$table;

    public function __construct(){
        parent::__construct();
    }

    public function setData($data = Array()){
        if (array_key_exists('loginas', $data)) {
            $this->loginas = $data['loginas'];
            if($data['loginas']=='admin'){
                $this->table='users';
                $_SESSION['loginas']='Admin';
            }
            else{
                $this->table='members';
                $_SESSION['loginas']='User';
            }
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('mobile_no', $data)) {
            //$this->password = md5($data['password']);
            $this->password =md5($data['mobile_no']);
        }
        return $this;
    }

    public function is_exist(){

        $query="SELECT * FROM members WHERE `cualumni`.`email` ='$this->email' ";
        $STH=$this->conn->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function is_registered(){
        

        $query = "SELECT * FROM ".$this->table." WHERE `email`='$this->email' AND password='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
}

