<?php
  include('header.php');
 ?>
 <style>
 .stepwizard-step p {
    margin-top: 10px;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.part {
  border: 1px solid #337ab7;
  padding: 10px;
  margin-bottom: 10px;
  border-radius: 6px;
}
.subtit {
  background: #337ab7;
  color: #fff;
  padding: 3px;
  text-align: center;
}
.marit {
  padding-top: 29px;
}
</style>
 <div class="">
   <div class="container">
     <div class="row">
       <div class="breadcrumb"><h6>Home / Assessment</h6></div>
     </div>
   </div>
 </div>
<!-- Start About area -->
  <div id="about" class="about-area">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="Mycon">
            
                    <div class="stepwizard"><!--col-md-6 col-md-offset-3-->
                        <div class="stepwizard-row setup-panel">
                          <div class="stepwizard-step">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p>Step 1</p>
                          </div>
                          <div class="stepwizard-step">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p>Step 2</p>
                          </div>
                          <div class="stepwizard-step">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p>Step 3</p>
                          </div>
                          <div class="stepwizard-step">
                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                            <p>Step 4</p>
                          </div>
                          <div class="stepwizard-step">
                            <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                            <p>Step 5</p>
                          </div>
                          <div class="stepwizard-step">
                            <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                            <p>Step 6</p>
                          </div>
                        </div>
                    </div>

                    <form role="form" action="" method="post" name="assessment">
                        <!--step 1 -->
                        <div class="row setup-content" id="step-1">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 1</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Assesment Sheet:</h6>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Assesment Sheet For</label>
                                      <input  maxlength="100" name="asfor" type="text" class="form-control" placeholder="Assesment Sheet For" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">File No.</label>
                                      <input  maxlength="100" name="fileNo" type="text" class="form-control" placeholder="File No." required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Reference Name</label>
                                      <input  maxlength="100" name="refEm" type="text" class="form-control" placeholder="Reference Name By Employer" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <label class="control-label">Reference Name</label>
                                          <select name="refSkygoal" class="form-control" onchange="myRef(this)" required>
                                            <option value="">Select Reference</option>
                                            <option value="Skygoal Synergy">Skygoal Synergy</option>
                                            <option value="Saifur Rahman Chowddhury">Saifur Rahman Chowddhury</option>
                                            <option value="Delwar Hossen">Delwar Hossen</option>
                                            <option value="Surendra Roka">Surendra Roka</option>
                                            <option value="Others">Others</option>
                                          </select>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="option" style="display: none;">
                                            <input  maxlength="100" name="refOther" type="text" class="form-control" placeholder="Other refference"/>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Consultant Name</label>
                                  <input  maxlength="100" type="text" name="consltName" class="form-control" placeholder="Consultant Name"  required/>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Apply Country Name</label>
                                      <select name="Applycountry" class="form-control" required>
                                        <option value="">Select Country</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Estonia">Estonia</option>
                                      </select>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Type of Work</label>
                                      <input  maxlength="100" name="toWork" type="text" class="form-control" placeholder="Type of Work" required/>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>

                              <fieldset class="part">
                                <h6 class="subtit">Passport Data :</h6>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Surname</label>
                                      <input  maxlength="100" name="surName" type="text" class="form-control" placeholder="Surname" onkeyup="FName()" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Given Name</label>
                                      <input  maxlength="100" name="givenName" type="text" class="form-control" placeholder="Given Name" onkeyup="FName()" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Candidate Full Name</label>
                                  <input  maxlength="100" name="fullName" id="Full_name" type="text" class="form-control" placeholder="Full Name" readonly="readonly" />
                                </div>  
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Father`s Name</label>
                                      <input  maxlength="100" name="fathersName" type="text" class="form-control" placeholder="Father`s Name" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Mother`s Name</label>
                                      <input  maxlength="100" name="mothersName" type="text" class="form-control" placeholder="Mother`s Name" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="marit">
                                        <label class="control-label">Marital Status</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="maritalStatus" value="Yes" onchange="maritStatus(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="maritalStatus" value="No" onchange="maritStatus(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="marit_option" style="display: none;">
                                        <label class="control-label">Spouse`s Name</label>
                                        <input  maxlength="100" name="spouseName" type="text" class="form-control" placeholder="Spouse`s Name" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <label class="control-label">Date Of Birth</label>
                                      <input  maxlength="100" name="dob" type="text" class="form-control datepicker" placeholder="Date Of Birth" required/>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">Age</label>
                                      <input  maxlength="100" name="age" type="text" class="form-control" placeholder="Age" required/>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">Home Postal/Zip code</label>
                                      <input  maxlength="100" name="pzcode" type="text" class="form-control" placeholder="Home Postal/Zip code" required/>
                                    </div>

                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Place Of Birth</label>
                                      <input  maxlength="100" name="pob" type="text" class="form-control" placeholder="Place Of Birth" required/>
                                    </div>
                                     <div class="col-md-6">
                                      <label class="control-label">Candidate`s Citizenship</label>
                                      <select name="citizenship" class="form-control" required>
                                        <option value="">Select Country</option>
                                        <option value="Bangladesh">Bangladesh</option>
                                        <option value="Nepal">Nepal</option>
                                        <option value="Sri Lanka">Sri Lanka</option>
                                        <option value="Bhutan">Bhutan</option>
                                        <option value="Afganistan">Afganistan</option>
                                        <option value="The Maldives">The Maldives</option>
                                        <option value="India">India</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <label class="control-label">Passport No</label>
                                      <input  maxlength="100" name="passportNo" type="text" class="form-control" placeholder="Running" required/>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">Old Passport No</label>
                                      <input  maxlength="100" name="passportNoOld" type="text" class="form-control" placeholder="Old" required/>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">Place of Issue</label>
                                      <select name="placeOfissue" class="form-control" required>
                                        <option value="">Select place</option>
                                        <option value="Dhaka">Dhaka [for Bangladesh]</option>
                                        <option value="Mofa">Mofa [for Nepal]</option>
                                        <option value="Colombo">Colombo [for Sri Lanka]</option>
                                        <option value="Thimpu">Thimpu [for Bhutan]</option>
                                        <option value="Kabul">Kabul [for Afganistan]</option>
                                        <option value="Male">Male [for The Maldives]</option>
                                        <option value="State">State of India [for India]</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Passport Issue Date</label>
                                      <input  maxlength="100" name="passportIssuedate" type="text" class="form-control datepicker" placeholder="Passport Issue Date" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Passport Expiry Date</label>
                                      <input  maxlength="100" name="passportExpirydate" type="text" class="form-control datepicker" placeholder="Passport Expiry Date" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">National ID</label>
                                  <input  maxlength="100" name="NID" type="text" class="form-control" placeholder="National ID" required/>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Permanent Address</label>
                                 <div class="row">
                                   <div class="col-md-6">
                                      <input  maxlength="100" name="permAddress1" type="text" class="form-control" placeholder="Address line 1 required"/>
                                   </div>
                                   <div class="col-md-6">
                                     <input  maxlength="100" name="permAddress2" type="text" class="form-control" placeholder="Address line 2"/>
                                   </div>
                                 </div><br/>
                                 <div class="row">
                                   <div class="col-md-3">
                                     <input type="text" name="post_office" class="form-control" placeholder="Post office" required>
                                   </div>
                                   <div class="col-md-3">
                                     <input type="text" name="zip_code" class="form-control" placeholder="Post code / Zip code" required>
                                   </div>
                                   <div class="col-md-3">
                                     <input type="text" name="state" class="form-control" placeholder="State/Province/Region" required>
                                   </div>
                                   <div class="col-md-3">
                                     <input type="text" name="country" class="form-control" placeholder="Country" required>
                                   </div>
                                 </div>

                                </div>

                                <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                        <!--step 2 -->
                        <div class="row setup-content" id="step-2">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 2</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Personal Data:</h6>
                                 <div class="form-group">
                                  <label class="control-label">Candidate`s Email ID</label>
                                  <input  maxlength="100" type="email" name="cemail" class="form-control" placeholder="Your Email ID"  required/>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Password</label>
                                      <input  maxlength="100" name="passwd" type="text" class="form-control" placeholder="Password" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Recovery ID/ Mobile No.</label>
                                      <input  maxlength="100" name="recvID" type="text" class="form-control" placeholder="Recovery ID/ Mobile No." required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Candidate`s Contact No</label>
                                      <input  maxlength="100" name="refEm" type="text" class="form-control" placeholder="Home Country" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Candidate`s Contact No</label>
                                      <input  maxlength="100" name="refSkygoal" type="text" class="form-control" placeholder="India/UAE" required/>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <fieldset class="part">
                                <h6 class="subtit">Educational Data:</h6>
                                 <div class="table-responsive">
                                  <table class="table table-striped table-bordered">
                                  <tr>
                                    <td>Degree</td>
                                    <td>Subject/Group</td>
                                    <td>Board</td>
                                    <td>Passing Year</td>
                                    <td>Result</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="form-group">
                                        <select name="degree_name1" class="form-control" required>
                                          <option value="">Select</option>
                                          <option value="S.S.C">S.S.C</option>
                                          <option value="H.S.C">H.S.C</option>
                                          <option value="Degree/Hons.">Degree/Hons.</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="group1" class="form-control" required>
                                          <option value="">Select</option>
                                          <option value="Science">Science</option>
                                          <option value="Humanities">Humanities</option>
                                          <option value="Business Studies">Business Studies</option>
                                          <option value="Business Studies">Business Studies</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                        <select name="board1" class="form-control" required>
                                          <option value="">Select</option>
                                          <option value="Dhaka">Dhaka</option>
                                          <option value="Chittagong">Chittagong</option>
                                          <option value="Comilla">Comilla</option>
                                          <option value="Rajshahi">Rajshahi</option>
                                          <option value="Jessore">Jessore</option>
                                          <option value="Barishal">Barishal</option>
                                          <option value="Dinajpur">Dinajpur</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Technical">Technical</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="passingYear1" class="form-control" required>
                                          <option value="">Select</option><option value="2019">2019</option>
                                          <option value="2018">2018</option><option value="2017">2017</option>
                                          <option value="2016">2016</option><option value="2015">2015</option>
                                          <option value="2014">2014</option><option value="2013">2013</option>
                                          <option value="2012">2012</option><option value="2011">2011</option>
                                          <option value="2010">2010</option><option value="2009">2009</option>
                                          <option value="2008">2008</option><option value="2007">2007</option>
                                          <option value="2006">2006</option><option value="2005">2005</option>
                                          <option value="2004">2004</option><option value="2003">2003</option>
                                          <option value="2002">2002</option><option value="2001">2001</option>
                                          <option value="2000">2000</option><option value="1999">1999</option>
                                          <option value="1998">1998</option><option value="1997">1997</option>
                                          <option value="1996">1996</option><option value="1995">1995</option>
                                          <option value="1994">1994</option><option value="1993">1993</option>
                                          <option value="1992">1992</option><option value="1991">1991</option>
                                          <option value="1999">1990</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <select name="result1" class="form-control" onchange="gpaFunctionA(this)">
                                              <option value="">Select</option>
                                              <option value="1st Division">1st Division</option>
                                              <option value="2nd Division">2nd Division</option>
                                              <option value="3rd Division">3rd Division</option>
                                              <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                              <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                            </select>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_one" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_two" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="form-group">
                                        <select name="degree_name2" class="form-control">
                                          <option value="">Select</option>
                                          <option value="S.S.C">S.S.C</option>
                                          <option value="H.S.C">H.S.C</option>
                                          <option value="Degree/Hons.">Degree/Hons.</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="group2" class="form-control">
                                          <option value="">Select</option>
                                          <option value="Science">Science</option>
                                          <option value="Humanities">Humanities</option>
                                          <option value="Business Studies">Business Studies</option>
                                          <option value="Business Studies">Business Studies</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                        <select name="board2" class="form-control">
                                          <option value="">Select</option>
                                          <option value="Dhaka">Dhaka</option>
                                          <option value="Chittagong">Chittagong</option>
                                          <option value="Comilla">Comilla</option>
                                          <option value="Rajshahi">Rajshahi</option>
                                          <option value="Jessore">Jessore</option>
                                          <option value="Barishal">Barishal</option>
                                          <option value="Dinajpur">Dinajpur</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Technical">Technical</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="passingYear2" class="form-control">
                                          <option value="">Select</option><option value="2019">2019</option>
                                          <option value="2018">2018</option><option value="2017">2017</option>
                                          <option value="2016">2016</option><option value="2015">2015</option>
                                          <option value="2014">2014</option><option value="2013">2013</option>
                                          <option value="2012">2012</option><option value="2011">2011</option>
                                          <option value="2010">2010</option><option value="2009">2009</option>
                                          <option value="2008">2008</option><option value="2007">2007</option>
                                          <option value="2006">2006</option><option value="2005">2005</option>
                                          <option value="2004">2004</option><option value="2003">2003</option>
                                          <option value="2002">2002</option><option value="2001">2001</option>
                                          <option value="2000">2000</option><option value="1999">1999</option>
                                          <option value="1998">1998</option><option value="1997">1997</option>
                                          <option value="1996">1996</option><option value="1995">1995</option>
                                          <option value="1994">1994</option><option value="1993">1993</option>
                                          <option value="1992">1992</option><option value="1991">1991</option>
                                          <option value="1999">1990</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <select name="result2" class="form-control" onchange="gpaFunctionB(this)">
                                              <option value="">Select</option>
                                              <option value="1st Division">1st Division</option>
                                              <option value="2nd Division">2nd Division</option>
                                              <option value="3rd Division">3rd Division</option>
                                              <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                              <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                            </select>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_three" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_four" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <div class="form-group">
                                        <select name="degree_name3" class="form-control">
                                          <option value="">Select</option>
                                          <option value="S.S.C">S.S.C</option>
                                          <option value="H.S.C">H.S.C</option>
                                          <option value="Degree/Hons.">Degree/Hons.</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="group3" class="form-control">
                                          <option value="">Select</option>
                                          <option value="Science">Science</option>
                                          <option value="Humanities">Humanities</option>
                                          <option value="Business Studies">Business Studies</option>
                                          <option value="Business Studies">Business Studies</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                        <select name="board3" class="form-control">
                                          <option value="">Select</option>
                                          <option value="Dhaka">Dhaka</option>
                                          <option value="Chittagong">Chittagong</option>
                                          <option value="Comilla">Comilla</option>
                                          <option value="Rajshahi">Rajshahi</option>
                                          <option value="Jessore">Jessore</option>
                                          <option value="Barishal">Barishal</option>
                                          <option value="Dinajpur">Dinajpur</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Madrasah">Madrasah</option>
                                          <option value="Technical">Technical</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <select name="passingYear3" class="form-control">
                                          <option value="">Select</option><option value="2019">2019</option>
                                          <option value="2018">2018</option><option value="2017">2017</option>
                                          <option value="2016">2016</option><option value="2015">2015</option>
                                          <option value="2014">2014</option><option value="2013">2013</option>
                                          <option value="2012">2012</option><option value="2011">2011</option>
                                          <option value="2010">2010</option><option value="2009">2009</option>
                                          <option value="2008">2008</option><option value="2007">2007</option>
                                          <option value="2006">2006</option><option value="2005">2005</option>
                                          <option value="2004">2004</option><option value="2003">2003</option>
                                          <option value="2002">2002</option><option value="2001">2001</option>
                                          <option value="2000">2000</option><option value="1999">1999</option>
                                          <option value="1998">1998</option><option value="1997">1997</option>
                                          <option value="1996">1996</option><option value="1995">1995</option>
                                          <option value="1994">1994</option><option value="1993">1993</option>
                                          <option value="1992">1992</option><option value="1991">1991</option>
                                          <option value="1999">1990</option>
                                        </select>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <select name="result3" class="form-control" onchange="gpaFunctionC(this)">
                                              <option value="">Select</option>
                                              <option value="1st Division">1st Division</option>
                                              <option value="2nd Division">2nd Division</option>
                                              <option value="3rd Division">3rd Division</option>
                                              <option value="GPA (Out of 4)">GPA (Out of 4)</option>
                                              <option value="GPA (Out of 5)">GPA (Out of 5)</option>
                                            </select>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_five" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA4" placeholder="Enter CGPA out of 4">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div id="CGPA_six" style="display: none;">
                                              <input type="text" class="form-control" name="CGPA5" placeholder="Enter CGPA out of 5">
                                            </div>
                                          </div>

                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                 </table>
                                </div>
                              </fieldset>
                              <fieldset class="part">
                                <h6 class="subtit">Experience Data:</h6>
                                 <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="marit">
                                        <label class="control-label">Work experience Certificate</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="maritalStatus" value="Yes" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="maritalStatus" value="No" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Company Name</label>
                                      <input  maxlength="100" name="companyName" type="text" class="form-control" placeholder="Company Name" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Company Address</label>
                                      <input  maxlength="100" name="companyAddress" type="text" class="form-control" placeholder="Company Address" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <label class="control-label">Type Of Work</label>
                                      <input  maxlength="100" name="typeOfwork" type="text" class="form-control" placeholder="Type Of Work" required/>
                                    </div>
                                    <div class="col-md-4">
                                      <label class="control-label">Work Duration</label>
                                      <input  maxlength="100" name="workDuration" type="text" class="form-control" placeholder="Work Duration" required/>
                                    </div>

                                    <div class="col-md-4">
                                      <label class="control-label">Months</label>
                                      <input  maxlength="100" name="months" type="text" class="form-control" placeholder="Months" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Company Email ID</label>
                                      <input  maxlength="100" name="comEmail" type="email" class="form-control" placeholder="Company Email ID" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Company Website</label>
                                      <input  maxlength="100" name="comWebsite" type="text" class="form-control" placeholder="Company Website" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <!--<a href="#step-1" type="button" class="btn btn-primary prevBtn btn-sm pull-left">Previous</a>-->
                                  </div>
                                  <div class="col-md-6">
                                    <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                                  </div>
                                </div>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                        <!--step 3 -->
                        <div class="row setup-content" id="step-3">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 3</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Travel Data:</h6>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="WB">
                                          <label class="control-label">Candidate have before work visa in another country</label>
                                          <label class="radio-inline">
                                            <input type="radio" name="work_before" value="Yes" onchange="beforeWork(this)" required> Yes
                                          </label>
                                          <label class="radio-inline">
                                            <input type="radio" name="work_before" value="No" onchange="beforeWork(this)" required> No
                                          </label>
                                        </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="workBefore" style="display: none;">
                                            <label class="control-label">Select Country</label>
                                            <select name="beforeWorkCountry" class="form-control" required>
                                              <option value="">Select</option>
                                              <option value="Bangladesh">Bangladesh</option>
                                              <option value="Nepal">Nepal</option>
                                              <option value="Sri Lanka">Sri Lanka</option>
                                              <option value="Bhutan">Bhutan</option>
                                              <option value="Afganistan">Afganistan</option>
                                              <option value="The Maldives">The Maldives</option>
                                              <option value="India">India</option>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="WB">
                                            <label class="control-label">Embassy Refuse before any Country?</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="refuse_embassy" value="Yes" onchange="refuseEmbassy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="refuse_embassy" value="No" onchange="refuseEmbassy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="refuseEmb" style="display: none;">
                                              <label class="control-label">Select Country :</label>
                                              <select name="refuseCountry" class="form-control" required>
                                                <option value="">Select</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Afganistan">Afganistan</option>
                                                <option value="The Maldives">The Maldives</option>
                                                <option value="India">India</option>
                                              </select>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div> 
                                
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Any visit visa another Countries</label>
                                         <select name="visitVanotherCountry" class="form-control" required>
                                            <option value="">Select Country</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Afganistan">Afganistan</option>
                                            <option value="The Maldives">The Maldives</option>
                                            <option value="India">India</option>
                                          </select>
                                    </div>
                                    <div class="col-md-6">
                                      
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="control-label">Last passport (If any)</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="lPassport" value="Yes" onchange="lastPassport(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="lPassport" value="No" onchange="lastPassport(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="lastPass" style="display: none;">
                                        <label class="control-label">Passport No. (if Lost)</label>
                                        <input  maxlength="100" name="lastPassNo" type="text" class="form-control" placeholder="Passport No. (if Lost)"/>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="control-label">Face Cutting (as like worker)</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="fcutting" value="Yes" onchange="face(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="fcutting" value="No" onchange="face(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="fcecut" style="display: none;">
                                        <label class="control-label">Upload Picture taken by Mobile Camera</label>
                                        <input type="file" name="faceimg" class="form-control" required>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <fieldset class="part">
                                <h6 class="subtit">Personal Documents Requirement Data :</h6>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Passport Copy:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="passportCopy" value="Yes" onchange="passCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="passportCopy" value="No" onchange="passCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="pcopy" style="display: none;">
                                            <input type="file" name="passport_copy" class="form-control" >
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Curriculum Vitae (C.V.):</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="cv" value="Yes" onchange="cvCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="cv" value="No" onchange="cvCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="cvcopies" style="display: none;">
                                            <input type="file" name="cv_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Birth Certificate:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="birthcerti" value="Yes" onchange="birthCert(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="birthcerti" value="No" onchange="birthCert(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="bcert" style="display: none;">
                                            <input type="file" name="birthcert_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Old Visa Copy (if any):</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="old_visa" value="Yes" onchange="oldVisa(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="old_visa" value="No" onchange="oldVisa(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Oldvisa" style="display: none;">
                                            <input type="file" name="oldvisa_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="control-label">NID No. :</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="NID" value="Yes" onchange="NIDcopy(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="NID" value="No" onchange="NIDcopy(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="NIDc" style="display: none;">
                                      <input  maxlength="100" name="nid_copy" type="text" class="form-control" placeholder="NID Number"/>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Photo 2 Copies:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="pic_copy" value="Yes" onchange="picTwocopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="pic_copy" value="No" onchange="picTwocopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="picture" style="display: none;">
                                            <input type="file" name="pic_copy1" class="form-control">
                                            <input type="file" name="pic_copy2" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Police clearence certificate:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="police_clearence" value="Yes" onchange="policeCert(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="police_clearence" value="No" onchange="policeCert(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="policeClrcert" style="display: none;">
                                            <input type="file" name="police_cert" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="control-label">Work experience Certificate :</label>
                                        <label class="radio-inline">
                                          <input type="radio" name="wec" value="Yes" onchange="workExpr(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="wec" value="No" onchange="workExpr(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="wexpr" style="display: none;">
                                      <input  maxlength="100" name="workEx_certificate" type="text" class="form-control" placeholder="Work experience Certificate "/>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Marriage certificate:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="marriage_cert" value="Yes" onchange="marriageCert(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="marriage_cert" value="No" onchange="marriageCert(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="marriage" style="display: none;">
                                            <input type="file" name="marriage_cert_copy" class="form-control" required>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Bank Statement:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="bank_statement" value="Yes" onchange="bankStatement(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="bank_statement" value="No" onchange="bankStatement(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Bstatement" style="display: none;">
                                            <input type="file" name="bank_statment_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Air ticket reservation:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="airTicResrv" value="Yes" onchange="airTicket(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="airTicResrv" value="No" onchange="airTicket(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Airticket" style="display: none;">
                                            <input type="file" name="airTicResrv_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Insurance:</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="insurnce" value="Yes" onchange="insuranceCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="insurnce" value="No" onchange="insuranceCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="insCopy" style="display: none;">
                                            <input type="file" name="insurance_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="control-label"><strong class="green">N.B.</strong>The Candidates Should have to agreed to work for the company for a minimum period of one year :</label>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="radio-inline">
                                          <input type="radio" name="wconditions" value="Yes" onchange="agreeToWork(this)" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="wconditions" value="No" agreeToWork onchange="agreeToWork(this)" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div id="wConditon" style="display: none;">
                                      <input  maxlength="100" name="agree_statement" type="text" class="form-control" placeholder=""/>
                                    </div>
                                    </div>
                                  </div>
                                </div> 
                                <div class="form-group">
                                  <label class="control-label">Impression for Candidates (in%)</label>
                                  <input  maxlength="100" type="text" name="impressionForPercentece" class="form-control" placeholder=""  required/>
                                </div> 
                                <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                        <!--step 4 -->
                        <div class="row setup-content" id="step-4">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 4</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Work Permit Data :</h6>
                                <div class="form-group">
                                  <label class="control-label">Applied Country Name</label>
                                  <select name="ApplycountryName" class="form-control" required>
                                        <option value="">Select Country</option>
                                        <option value="Poland">Poland</option>
                                        <option value="Hungary">Hungary</option>
                                        <option value="Czech Republic">Czech Republic</option>
                                        <option value="Estonia">Estonia</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                  <label class="control-label">Mentioned Company Name (if work permit provided by Agent)</label>
                                  <input  maxlength="100" type="text" name="mentionCompanyName" class="form-control" placeholder="" required />
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Working hours:</label>
                                            <select name="workingHours" class="form-control" onchange="wrkingHour(this)" required>
                                                <option value="">Select</option>
                                                <option value="[Hours per week] /Week">[Hours per week] /Week</option>
                                                <option value="[Hours per month] /Month">[Hours per month] /Month</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="wrkWeek" style="display: none;">
                                            <input type="text" name="workinghour_weekly" class="form-control" placeholder="Hour per week">
                                          </div>
                                          <div id="wrkMonth" style="display: none;">
                                            <input type="text" name="workinghour_monthly" class="form-control" placeholder="Hour per month">
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Salary:</label>
                                            <select name="workingHours" class="form-control" onchange="salaryHour(this)" required>
                                                <option value="">Select</option>
                                                <option value="[Salary per week] /Week">[Salary per week] /Week</option>
                                                <option value="[Salary per month] /Month">[Salary per month] /Month</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="salaryWeek" style="display: none;">
                                            <input type="text" name="salary_weekly" class="form-control" placeholder="Salary per week">
                                          </div>
                                          <div id="salaryMonth" style="display: none;">
                                            <input type="text" name="salary_monthly" class="form-control" placeholder="Salary per month">
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Salary Per Month (PLN.)</label>
                                      <input  maxlength="100" name="salaryPermonth" type="text" class="form-control" placeholder="Salary Per Month (PLN.)" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Salary (PLN=BD./Nepalis)</label>
                                      <input  maxlength="100" name="salaryNepails" type="text" class="form-control" placeholder="Salary (PLN=BD./Nepalis)" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Salary Included :</label>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                      <input  maxlength="100" name="salayWithtax" type="text" class="form-control" placeholder="Salary Included with Tax " required/>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="">
                                      <input  maxlength="100" name="salayWithouttax" type="text" class="form-control" placeholder="Salary Included Without Tax required"/>
                                    </div>
                                    </div>
                                  </div>
                                </div> 
                                <div class="form-group">
                                  <label class="control-label">Overtime :</label>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                      <input  maxlength="100" name="overtime" type="text" class="form-control" placeholder="Overtime" required/>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="">
                                      <input  maxlength="100" name="overtimePerhour" type="text" class="form-control" placeholder="Pln. (zl) Per Hour" required/>
                                    </div>
                                    </div>
                                  </div>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Accommodation (Provided by the Company) :</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="accCompany" value="Yes" onchange="accomCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="accCompany" value="No" onchange="accomCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="accom" style="display: none;">
                                            <input type="file" name="accom_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Resident Permit (Resident Permit will be provided by the Company) :</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="residentPermit" value="Yes" onchange="residntCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="residentPermit" value="No" onchange="residntCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="resident" style="display: none;">
                                            <input type="file" name="resident_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Food (Provided by the Company) :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="food" value="Yes" onchange="foodbyCompany(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="food" value="No" onchange="foodbyCompany(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="foodcopy" style="display: none;">
                                            <input type="file" name="food_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Medical (Provided by the Company) :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="medical" value="Yes" onchange="medicalbyCompany(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="medical" value="No" onchange="medicalbyCompany(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="medicalcopy" style="display: none;">
                                            <input type="file" name="medical_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Transportation (Provided by the Company) :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="transport" value="Yes" onchange="transbyCompany(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="transport" value="No" onchange="transbyCompany(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="transcopy" style="display: none;">
                                            <input type="file" name="transport_copy" class="form-control" required>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Insurance (Provided by the Company) :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="insrnce" value="Yes" onchange="insrncebyCompany(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="insrnce" value="No" onchange="insrncebyCompany(this)" nrequired> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="insrncecopy" style="display: none;">
                                            <input type="file" name="insrnce_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Other Terms and Conditions (According the polish civil code) :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="terms" value="Yes" onchange="termAndcondition(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="terms" value="No" onchange="termAndcondition(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="termCon" style="display: none;">
                                            <input type="file" name="termCondition_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                  </div>
                                </div>
                                 <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                        <!--step 5 -->
                        <div class="row setup-content" id="step-5">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 5</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Work Permit Data Verification :</h6>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <div class="marit">
                                        <label class="control-label">Received Email Copy :</label>
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="row">
                                              <div class="col-md-7">
                                                <div class="checkbox">
                                                  <label>
                                                    <input type="checkbox" id="chk1" value="Work Permit">
                                                     Work Permit
                                                  </label><br/><br/>
                                                  <label>
                                                    <input type="checkbox" id="chk2" value="Accommodation Letter" onchange="checkB(this)">
                                                     Accommodation Letter
                                                  </label><br/><br/>
                                                  <label>
                                                    <input type="checkbox" id="chk3" value="Guarantee Letter" onchange="checkC(this)">
                                                    Guarantee Letter  
                                                  </label><br/><br/>
                                                   <label>
                                                    <input type="checkbox" id="chk4" value="Others" onchange="checkD(this)">
                                                    Others 
                                                  </label>
                                                </div>
                                              </div>
                                              <div class="col-md-5">
                                                <div class="row">
                                                  <div class="col-md-12" id="wpermitcopy" style="display: none;">
                                                    <input type="file" name="work_permit_copy" class="form-control">
                                                  </div><br/><br/>
                                                  <div class="col-md-12" id="accomLetter" style="display: none;">
                                                    <input type="file" name="work_accom_copy" class="form-control">
                                                  </div><br/><br/>
                                                  <div class="col-md-12" id="gaurnteeLetter" style="display: none;">
                                                    <input type="file" name="work_gaurntee_copy" class="form-control">
                                                  </div><br/><br/>
                                                  <div class="col-md-12" id="wothers" style="display: none;">
                                                    <input type="file" name="work_others_copy" class="form-control">
                                                  </div>

                                                </div>
                                              </div>
                                            </div>
                                            
                                          </div>
                                          <div class="col-md-6">
                                            <div class="row">
                                              <div class="col-md-7">
                                                <div class="checkbox">
                                                  <label>
                                                    <input type="checkbox" id="chk5" value="Contract Paper" onchange="checkE(this)" >
                                                       Contract Paper
                                                  </label><br/><br/>
                                                  <label>
                                                    <input type="checkbox" id="chk6" value="Extention Letter" onchange="checkF(this)">
                                                     Extention Letter
                                                  </label><br/><br/>
                                                  <label>
                                                    <input type="checkbox" id="chk7" value="KRS Paper" onchange="checkG(this)">
                                                    KRS Paper
                                                  </label>
                                                </div>
                                              </div>
                                              <div class="col-md-5">
                                                <div class="row">
                                                  <div class="col-md-12" id="wcontractcopy" style="display: none;">
                                                    <input type="file" name="work_contract_copy" class="form-control">
                                                  </div><br/><br/>
                                                  <div class="col-md-12" id="wextnLetter" style="display: none;">
                                                    <input type="file" name="work_extensionletter_copy" class="form-control">
                                                  </div><br/><br/>
                                                  <div class="col-md-12" id="krsPaper" style="display: none;">
                                                    <input type="file" name="work_krsPaper_copy" class="form-control">
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Work Permit Issue Date</label>
                                      <input  maxlength="100" name="wpissueDate" type="text" class="form-control datepicker" placeholder="Work Permit Issue Date" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Days Passed (as Today)</label>
                                      <input  maxlength="100" name="daysPassed" type="text" class="form-control" placeholder="Days Passed (as Today)" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Period of contract (work permit)</label>
                                      <input  maxlength="100" name="wpissueDate" type="text" class="form-control" placeholder="From" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">To</label>
                                      <input  maxlength="100" name="daysPassed" type="text" class="form-control" placeholder="To" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Company Website</label>
                                      <input  maxlength="100" name="comWebsite" type="text" class="form-control" placeholder="Company Website" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Remarks for Company (after Video Verification)</label>
                                      <input  maxlength="100" name="comRemarks" type="text" class="form-control" placeholder="Remarks for Company" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Any Previous Employee from South Asia </label>
                                      <input  maxlength="100" name="prevEmployee" type="text" class="form-control" placeholder="Any Previous Employee from South Asia " required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Any Present Employee from South Asia</label>
                                      <input  maxlength="100" name="presentEmployee" type="text" class="form-control" placeholder="Any Present Employee from South Asia" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Tax Solvency of Company :</label>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                        <label class="radio-inline">
                                          <input type="radio" name="taxSolvency" value="Yes" required> Yes
                                        </label>
                                        <label class="radio-inline">
                                          <input type="radio" name="taxSolvency" value="No" required> No
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="">
                                      <input  maxlength="100" name="taxSolvencydetails" type="text" class="form-control" placeholder="" required/>
                                    </div>
                                    </div>
                                  </div>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Impression for Company(in %)</label>
                                      <input  maxlength="100" name="impression" type="text" class="form-control" placeholder="Impression for Company(in %)" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Remarks for Company</label>
                                      <input  maxlength="100" name="RemrkCompany" type="text" class="form-control" placeholder="Remarks for Company" required/>
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                              <fieldset class="part">
                                <h6 class="subtit">Required Company Documents :</h6>
                                <div class="form-group">
                                  <label class="control-label">Hard/Original Copy of Permit</label>
                                  <input  maxlength="100" type="text" name="copyOfpermit" class="form-control" placeholder=""  required/>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Contract Paper :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="terms" value="Yes" onchange="conPaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="terms" value="No" onchange="conPaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="conpaper" style="display: none;">
                                            <input type="file" name="contractpaper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Accommodation Paper :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="accomo_paper" value="Yes" onchange="AccPaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="accomo_paper" value="No" onchange="AccPaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Accomopaper" style="display: none;">
                                            <input type="file" name="accomo_paper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Guarantee Paper :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="guarantee_paper" value="Yes" onchange="GuarnteePaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="guarantee_paper" value="No" onchange="GuarnteePaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Gaurpaper" style="display: none;">
                                            <input type="file" name="gauntee_paper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Extention Paper :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="ext_paper" value="Yes" onchange="ExtPaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="ext_paper" value="No" onchange="ExtPaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="Ext" style="display: none;">
                                            <input type="file" name="ext_paper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">KRS Paper :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="ext_paper" value="Yes" onchange="KRSPaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="ext_paper" value="No" onchange="KRSPaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="KRS" style="display: none;">
                                            <input type="file" name="KRS_paper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="marit">
                                            <label class="control-label">Company Tax Payment Details :</label><br/>
                                            <label class="radio-inline">
                                              <input type="radio" name="com_tax" value="Yes" onchange="compTaxPaper(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="com_tax" value="No" onchange="compTaxPaper(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div id="comTax" style="display: none;">
                                            <input type="file" name="comTax_paper_copy" class="form-control">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label">Others</label>
                                  <input  maxlength="100" type="text" name="permitOthers" class="form-control" placeholder=""  required/>
                                </div> 
                                <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                        <!--step 6 -->
                        <div class="row setup-content" id="step-6">
                          <div class="">
                            <div class="col-md-12">
                              <h3> Step 6</h3>
                              <fieldset class="part">
                                <h6 class="subtit">Submission Data :</h6>
                                 <div class="form-group">
                                  <label class="control-label">Intended date of arrival to the Republic of Poland</label>
                                  <input  maxlength="100" type="text" name="intendedDate" class="form-control datepicker" placeholder=""  required/>
                                </div> 
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Intended date of departure from the Republic of Poland (as Work Permit)</label>
                                      <input  maxlength="100" name="departureDate" type="text" class="form-control datepicker" placeholder="" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Duration of the Intended stay of transit indicate number of days</label>
                                      <input  maxlength="100" name="durationOfintend" type="text" class="form-control" placeholder="" required />
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <label class="control-label">Submission Date</label>
                                      <input  maxlength="100" name="submissionDate" type="text" class="form-control datepicker" placeholder="Submission Date" required/>
                                    </div>
                                    <div class="col-md-6">
                                      <label class="control-label">Remarks (Before Submission)</label>
                                      <input  maxlength="100" name="RemarksBeforeSumbission" type="text" class="form-control" placeholder="Remarks (Before Submission)" required/>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <div class="">
                                      <label class="control-label">Possibility (%)</label>
                                      <input  maxlength="100" name="Possibility" type="text" class="form-control" placeholder="Possibility (%)" required/>
                                    </div>
                                    </div>                                 
                                    <div class="col-md-4">
                                        <div class="">
                                        <label class="control-label">Token no.</label>
                                        <input name="tokenNo" type="file" class="form-control" required/>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="">
                                            <label class="control-label">Get Visa :</label>
                                            <label class="radio-inline">
                                              <input type="radio" name="getVisa" value="Yes" onchange="visaCopy(this)" required> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="getVisa" value="No" onchange="visaCopy(this)" required> No
                                            </label>
                                          </div>
                                        </div>
                                        <div class="col-md-12" id="visacopy" style="display: none;">
                                          <input type="file" name="visa_copy" class="form-control" required>
                                        </div>
                                      </div>
                                        
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="">
                                      <label class="control-label">Refugee no.</label>
                                      <input  maxlength="100" name="RefugeeNo" type="text" class="form-control" placeholder="Refugee no." required/>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="">
                                      <label class="control-label">Remarks (after refugee)</label>
                                      <input  maxlength="100" name="afterRefugee" type="text" class="form-control" placeholder="emarks (after refugee)" required/>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                  <div class="form-group">
                                    <div class="">
                                      <label class="control-label">Note :</label>
                                      <textarea name="note" rows="2" cols="3" class="form-control" placeholder="Note"></textarea>
                                    </div>
                                  </div>
                                  <button class="btn btn-success btn-sm pull-right" type="submit" name="submit">Submit</button>
                              </fieldset>
                            </div>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<script>
  //step1 : refference others filed 
  function myRef(that) {
    if (that.value == "Others") {
      //alert("check");
      document.getElementById("option").style.display = "block";
    } else {
      document.getElementById("option").style.display = "none";
    }
  }
  //step 1: passport data full name
  function FName(){
    var num1 = document.assessment.surName.value;
    var num2 = document.assessment.givenName.value;
    var space =" ";
    var sum =num1+space+num2;
    document.getElementById('Full_name').value = sum;
  }
  //step1 : Marital status with hidden spouse name 
  function maritStatus(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("marit_option").style.display = "block";
    } else {
      document.getElementById("marit_option").style.display = "none";
    }
  }

//step2 : CGPA Result
  function gpaFunctionA(that) {
    //gpa out of 4
    if (that.value == "GPA (Out of 4)") {
      
      document.getElementById("CGPA_one").style.display = "block";
    } else {
      document.getElementById("CGPA_one").style.display = "none";
    }
    //gpa out of 5
    if (that.value == "GPA (Out of 5)") {
      
      document.getElementById("CGPA_two").style.display = "block";
    } else {
      document.getElementById("CGPA_two").style.display = "none";
    }
  }
function gpaFunctionB(that) {
    //gpa out of 4
    if (that.value == "GPA (Out of 4)") {
      
      document.getElementById("CGPA_three").style.display = "block";
    } else {
      document.getElementById("CGPA_three").style.display = "none";
    }
    //gpa out of 5
    if (that.value == "GPA (Out of 5)") {
      
      document.getElementById("CGPA_four").style.display = "block";
    } else {
      document.getElementById("CGPA_four").style.display = "none";
    }
  }
function gpaFunctionC(that) {
    //gpa out of 4
    if (that.value == "GPA (Out of 4)") {
      
      document.getElementById("CGPA_five").style.display = "block";
    } else {
      document.getElementById("CGPA_five").style.display = "none";
    }
    //gpa out of 5
    if (that.value == "GPA (Out of 5)") {
      
      document.getElementById("CGPA_six").style.display = "block";
    } else {
      document.getElementById("CGPA_six").style.display = "none";
    }
  }

 //step3 : Before work country 
  function beforeWork(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("workBefore").style.display = "block";
    } else {
      document.getElementById("workBefore").style.display = "none";
    }
  }
//step3 : Embassy refused country 
  function refuseEmbassy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("refuseEmb").style.display = "block";
    } else {
      document.getElementById("refuseEmb").style.display = "none";
    }
  }
//step3 : Last Passport 
  function lastPassport(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("lastPass").style.display = "block";
    } else {
      document.getElementById("lastPass").style.display = "none";
    }
  }
//step3 : Face cutting
  function face(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("fcecut").style.display = "block";
    } else {
      document.getElementById("fcecut").style.display = "none";
    }
  }
//step3 : Passport copy
  function passCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("pcopy").style.display = "block";
    } else {
      document.getElementById("pcopy").style.display = "none";
    }
  }
//step3 : CV copy
  function cvCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("cvcopies").style.display = "block";
    } else {
      document.getElementById("cvcopies").style.display = "none";
    }
  }
//step3 : Birth Certificate
  function birthCert(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("bcert").style.display = "block";
    } else {
      document.getElementById("bcert").style.display = "none";
    }
  }
//step3 : Birth Certificate
  function oldVisa(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Oldvisa").style.display = "block";
    } else {
      document.getElementById("Oldvisa").style.display = "none";
    }
  }
//step3 : NID Copy
  function NIDcopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("NIDc").style.display = "block";
    } else {
      document.getElementById("NIDc").style.display = "none";
    }
  }
//step3 : picture 2 Copy
  function picTwocopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("picture").style.display = "block";
    } else {
      document.getElementById("picture").style.display = "none";
    }
  }
//step3 : Police clearence
  function policeCert(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("policeClrcert").style.display = "block";
    } else {
      document.getElementById("policeClrcert").style.display = "none";
    }
  }
//step3 : Work experience
  function workExpr(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("wexpr").style.display = "block";
    } else {
      document.getElementById("wexpr").style.display = "none";
    }
  }
//step3 : Marriage certificate
  function marriageCert(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("marriage").style.display = "block";
    } else {
      document.getElementById("marriage").style.display = "none";
    }
  }
//step3 :Bank Statement
  function bankStatement(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Bstatement").style.display = "block";
    } else {
      document.getElementById("Bstatement").style.display = "none";
    }
  }
//step3 : Air ticket reservation
  function airTicket(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Airticket").style.display = "block";
    } else {
      document.getElementById("Airticket").style.display = "none";
    }
  }
//step3 : Insurance
  function insuranceCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("insCopy").style.display = "block";
    } else {
      document.getElementById("insCopy").style.display = "none";
    }
  }
//step3 : Work condition
  function agreeToWork(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("wConditon").style.display = "block";
    } else {
      document.getElementById("wConditon").style.display = "none";
    }
  }


//step4 : Hours per week/month
function wrkingHour(that) {
    //gpa out of 4
    if (that.value == "[Hours per week] /Week") {
      
      document.getElementById("wrkWeek").style.display = "block";
    } else {
      document.getElementById("wrkWeek").style.display = "none";
    }
    //gpa out of 5
    if (that.value == "[Hours per month] /Month") {
      
      document.getElementById("wrkMonth").style.display = "block";
    } else {
      document.getElementById("wrkMonth").style.display = "none";
    }
  }
//step 4: Salary per week/month
function salaryHour(that) {
    //gpa out of 4
    if (that.value == "[Salary per week] /Week") {
      
      document.getElementById("salaryWeek").style.display = "block";
    } else {
      document.getElementById("salaryWeek").style.display = "none";
    }
    //gpa out of 5
    if (that.value == "[Salary per month] /Month") {
      
      document.getElementById("salaryMonth").style.display = "block";
    } else {
      document.getElementById("salaryMonth").style.display = "none";
    }
  }

//step4 : Accomondation copy
  function accomCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("accom").style.display = "block";
    } else {
      document.getElementById("accom").style.display = "none";
    }
  }
//step4 : Resident copy
  function residntCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("resident").style.display = "block";
    } else {
      document.getElementById("resident").style.display = "none";
    }
  }
//step4 : Food by company
  function foodbyCompany(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("foodcopy").style.display = "block";
    } else {
      document.getElementById("foodcopy").style.display = "none";
    }
  }
//step4 : Medical by company
  function medicalbyCompany(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("medicalcopy").style.display = "block";
    } else {
      document.getElementById("medicalcopy").style.display = "none";
    }
  }
//step4 : Transport by company
  function transbyCompany(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("transcopy").style.display = "block";
    } else {
      document.getElementById("transcopy").style.display = "none";
    }
  }
//step4 : Insurence by company
  function insrncebyCompany(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("insrncecopy").style.display = "block";
    } else {
      document.getElementById("insrncecopy").style.display = "none";
    }
  }
//step4 : Terms & condition by company
  function termAndcondition(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("termCon").style.display = "block";
    } else {
      document.getElementById("termCon").style.display = "none";
    }
  }



//step5 : Check Option [Received email copy]
  $(function () {
        $("#chk1").click(function () {
            if ($(this).is(":checked")) {
                $("#wpermitcopy").show();
            } else {
                $("#wpermitcopy").hide();
            }
        });
    });


/*
  function checkA(that) {
    if (that.value == "Work Permit") {
      //alert("check");
      document.getElementById("wpermitcopy").style.display = "block";
    } else {
      document.getElementById("wpermitcopy").style.display = "none";
    }
  }
function checkB(that) {
    if (that.value == "Accommodation Letter") {
      //alert("check");
      document.getElementById("accomLetter").style.display = "block";
    } else {
      document.getElementById("accomLetter").style.display = "none";
    }
  }
function checkC(that) {
    if (that.value == "Guarantee Letter") {
      //alert("check");
      document.getElementById("gaurnteeLetter").style.display = "block";
    } else {
      document.getElementById("gaurnteeLetter").style.display = "none";
    }
  }
function checkD(that) {
    if (that.value == "Others") {
      //alert("check");
      document.getElementById("wothers").style.display = "block";
    } else {
      document.getElementById("wothers").style.display = "none";
    }
  }
function checkE(that) {
    if (that.value == "Contract Paper") {
      //alert("check");
      document.getElementById("wcontractcopy").style.display = "block";
    } else {
      document.getElementById("wcontractcopy").style.display = "none";
    }
  }
function checkF(that) {
    if (that.value == "Extention Letter") {
      //alert("check");
      document.getElementById("wextnLetter").style.display = "block";
    } else {
      document.getElementById("wextnLetter").style.display = "none";
    }
  }
function checkG(that) {
    if (that.value == "KRS Paper") {
      //alert("check");
      document.getElementById("krsPaper").style.display = "block";
    } else {
      document.getElementById("krsPaper").style.display = "none";
    }
  }

*/


//step5 : Contract Paper
  function conPaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("conpaper").style.display = "block";
    } else {
      document.getElementById("conpaper").style.display = "none";
    }
  }
//step5 : Accommodation Paper
  function AccPaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Accomopaper").style.display = "block";
    } else {
      document.getElementById("Accomopaper").style.display = "none";
    }
  }
//step5 : Guarantee Paper
  function GuarnteePaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Gaurpaper").style.display = "block";
    } else {
      document.getElementById("Gaurpaper").style.display = "none";
    }
  }
//step5 : Extension Paper
  function ExtPaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("Ext").style.display = "block";
    } else {
      document.getElementById("Ext").style.display = "none";
    }
  }
//step5 : KRS Paper
  function KRSPaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("KRS").style.display = "block";
    } else {
      document.getElementById("KRS").style.display = "none";
    }
  }
//step5 : Company tax payment Paper
  function compTaxPaper(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("comTax").style.display = "block";
    } else {
      document.getElementById("comTax").style.display = "none";
    }
  }
//step6 : Visa copy
  function visaCopy(that) {
    if (that.value == "Yes") {
      //alert("check");
      document.getElementById("visacopy").style.display = "block";
    } else {
      document.getElementById("visacopy").style.display = "none";
    }
  }

</script>

  <!-- End About area -->
 
  <?php
  include('footer.php');
 ?>