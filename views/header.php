<?php
include_once('../vendor/autoload.php');
if(!isset($_SESSION) ) session_start();

use App\User\User;
use App\User\Auth;
use App\Rbac\Role;
use App\Rbac\PrivilegedUser;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();

$obj->setData($_SESSION);
if(isset($_GET)){
  $obj->setData($_GET);
  if($_GET['downloadcard']=='yes'){
    $auth= new Auth();
    $status= $auth->setData($_GET)->is_registered();
  }
}


$User = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$objController = new \App\Controller\Controller();
$singleUser=$objController->objectToArray($User);

$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Skygoal Synergy &#8211; VISA PROCESSING CONSULTING FIRM IN INDIA, POLAND &amp; UAE</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/fav.png" rel="icon" width="50px" height="auto">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Alfa+Slab+One" rel="stylesheet"> 

  
  <!-- lightbox new -->
   <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="../resource/css/photogall.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
  <!-- lightbox new -->
  
  <!-- Bootstrap CSS File -->
  <link href="../resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!--owl slider -->
  <link rel='stylesheet' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/43033/owl.carousel.css'>

  <!-- Libraries CSS Files -->
  <link href="../resource/lib/nivo-slider/css/nivo-slider.css" rel="stylesheet">
  <link href="../resource/lib/owlcarousel/owl.carousel.css" rel="stylesheet">
  <link href="../resource/lib/owlcarousel/owl.transitions.css" rel="stylesheet">
  <link href="../resource/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../resource/lib/animate/animate.min.css" rel="stylesheet">
  <link href="../resource/lib/venobox/venobox.css" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="../resource/css/nivo-slider-theme.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="../resource/css/style.css" rel="stylesheet">
  <link href="../resource/css/oline.css" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="../resource/css/responsive.css" rel="stylesheet">
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>

<body data-spy="scroll" data-target="#navbar-example">
  <div class="container">
  <div class="col-md-12 text-right tittle">
    <p><b>
        <?php if($status){echo "Hi !"; echo $singleUser['fullName'] ; ?>
        !</b> &nbsp; <a class="tittle" href="User/Authentication/logout.php">[ Log Out ]</a>
    <?php
    }
    else {
      ?>
       <a class="tittle" href="login.php">[ Log in ]</a>|
      &nbsp;<a class="tittle" href="register.php">[ Register ]</a>
      <?php
    }
      ?>


    </p>
    <p id='demo4' class="text-right cal-container"></p>
  </div>
  </div>
<!--
  <div id="preloader"></div>
 -->

  <header>
    <!-- header-area start -->
    
    <div id="" class=""> <!--header-area-->
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-8">
                <div class="logo">
                    <a href="#">
                      <img src="../resource/img/logo.png" class="img img-responsive" width="40%" height="auto" alt="" title="">
                    </a>

                </div>
              </div>
              <div class="col-md-4">

              </div>
            </div>
          </div>
          
          <div class="col-md-4 text-right">
            <div class="social">
              <div class="sall">


              </div>
              <div class="soc">
                <div class="row">
                 
                  <div class="col-md-12">
                    <div class="row">

                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

        </div>
        <div class="row">



          <div class="col-md-12 col-sm-12 Tmenu">
            <!-- Navigation -->
            <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
										<!--<span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->
                    <i style="color: black !important;" class="fa fa-bars"></i>
									</button>
                <!-- Brand -->
                <!--<a class="navbar-brand page-scroll sticky-logo" href="index.php">
                  <h1><span>e</span>Business</h1>
                   Uncomment below if you prefer to use an image logo 
                 <img src="img/logo.png" class="img img-responsive" width="100%" height="auto" alt="" title="">
								</a>-->
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                <ul class="nav navbar-nav">
                  <?php
                  if($status){
                    $menus=$objController->objectToArray($obj->userMenus());
                    echo $menus['menu'];
                  }
                  ?>

                </ul>

              </div>
              <!-- navbar-collapse -->
            </nav>
            <!-- END: Navigation -->
          </div>
        </div>
      </div>
    </div>
    <!-- header-area end -->
  </header>
  <!-- header end -->
<div class="container">
  <div class="row">
    <div class="col-sm-12">
       <?php
       echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>";
       ?>
    </div>
  </div>
</div>
