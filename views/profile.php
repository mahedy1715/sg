<?php
ob_start();
use Mpdf\Utils\UtfString;
include('header.php');
include('session.php');
include_once('printscript.php');

$objController = new App\Controller\Controller();

$objController->setData($_SESSION);
if(isset($_GET)){
  $objController->setData($_GET);
}
//var_dump($_GET);
//$objAlumni->setData($_SESSION);
$objToArrayProfile=$objController->view();

if($objToArrayProfile){   // $_SESSION['pdf']='pdf';
}
//var_dump($objToArrayProfile);

$id=$objToArrayProfile['id'];
$password=$objToArrayProfile['password'];
$fullName=$objToArrayProfile['fullName'];
$fatherName=$objToArrayProfile['fatherName'];
$motherName=$objToArrayProfile['motherName'];
$addressLine1=$objToArrayProfile['addressLine1'];
$addressLine2=$objToArrayProfile['addressLine2'];
$city=$objToArrayProfile['city'];
$state=$objToArrayProfile['state'];
$postcode=$objToArrayProfile['postcode'];
$country=$objToArrayProfile['country'];
$email=$objToArrayProfile['email'];
$phoneNumber=$objToArrayProfile['phoneNumber'];
$dob=$objToArrayProfile['dob'];
$gender=$objToArrayProfile['gender'];
$passport=$objToArrayProfile['passport'];
$birthcert=$objToArrayProfile['birthcert'];
$passport_file=$objToArrayProfile['passport_file'];
$picture=$objToArrayProfile['picture'];
$brc=$objToArrayProfile['brc'];
$cv=$objToArrayProfile['cv'];
$comment=$objToArrayProfile['comment'];
$tmpPassword=$_SESSION['tmp_password'];



$html=<<<PDF
 <div   class="container ">
          <div align="center" class="row cardcenter">
              <div id="divprintview" style="width:595px; border:1px solid #CCCCCC;border-radius:5px;padding-left:30px; margin-bottom:10px;" class="divprintview profile col-md-6 col-md-offset-3">
                  <div class="text-center up cardcenter">
                      <h3 class="titA cardcenter">Skygoal Synergy</strong><br/></h3>
                      <p class="cardcenter">
                      VISA PROCESSING CONSULTING FIRM IN INDIA, POLAND & UAE
                     </p>
                    </div>
                  <div align="center"  class="row table-responsive">
                      <table  class="table">
                          <thead>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><u><b>Registration</b></u></td>
                          </tr>
                          <tr>
                              <td class="cardcenter" style="text-align: center;" colspan="3"><b>Applicant 's Information</b><br>
                                              <img width="192" src="uploads/$picture">
                              </td>
                          </tr>
                          <tr>
                              <td> Applicant Name</td> <td>:</td><td >$fullName</td>
                          </tr>
                          </thead>
                          <tr>
                            <td>Father</td> <td>:</td><td>$fatherName</td>
                          </tr>
                          <tr>
                            <td>Mother</td>
                            <td>:</td>
                            <td>$motherName</td>
                          </tr>
                          <tr>
                            <td>Address</td> <td>:</td><td>$addressLine1 <br> $addressLine2</td>
                          </tr>
                          <tr>
                            <td>City</td> <td>:</td><td> $city , $postcode  </td>
                          </tr>
                          <tr>
                              <td>State</td> <td>:</td> <td >$state</td>
                          </tr>
                          <tr>
                              <td>Country</td> <td>:</td> <td > $country </td>
                          </tr>
                          <tr>
                              <td>Email</td> <td>:</td> <td > $email </td>
                          </tr>
                          <tr>
                              <td>Mobile</td> <td>:</td> <td > $phoneNumber </td>
                          </tr>
                          <tr>
                              <td>Date of Birth</td> <td>:</td> <td > $dob </td>
                          </tr>
                          <tr>
                              <td>Gender</td> <td>:</td> <td > $gender </td>
                          </tr>
                           <tr>
                              <td>Passport No</td> <td>:</td> <td > $passport </td>
                          </tr>
                           <tr>
                              <td>Birth Certificate No</td> <td>:</td> <td > $birthcert </td>
                          </tr>
                          <tr>
                              <td>Passport</td> <td>:</td> <td > <a target="_blank" href="uploads/$passport_file">View</a> </td>
                          </tr>
                          <tr>
                              <td>Birth Certificate</td> <td>:</td> <td > <a target="_blank" href="uploads/$brc">View</a> </td>
                          </tr>
                           <tr>
                              <td>CV</td> <td>:</td> <td > <a target="_blank" href="uploads/$cv">View</a> </td>
                          </tr>
                           <tr>
                              <td>Note</td> <td>:</td> <td > $comment </td>
                          </tr>
                      </table>
                  </div>
              </div>
          </div>
        </div>
PDF;
?>
  <div class="container">
    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class=" col-md-4">
            <div class="inner-heading">

            </div>
          </div>
          <div class=" col-md-8">
            <ul style="background-color:inherit;" class="breadcrumb">
              <li><a href="home.php"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>

            </ul>
          </div>
        </div>
      </div>
    </section>
    <div id="dvContainer">
      <style>
        <?php
                include ('../resource/css/printsetup.css');
        ?>
      </style>
      <section id="content">

        <?php
        echo $html;
        ?>

      </section>
    </div>
    <div class="dpdf text-center">
      <button type="button" id="btnPrint"  class=" btn btn-dark">Print your card</button><br>
      <a href="profile.php?dpdf=<?php echo $_SESSION['pdf'];?>">Download As PDF</a>
    </div>


  </div>
<?php

//if(isset($_GET['pdf'])){
include_once ('../vendor/autoload.php');
//$printsetup = file_get_contents('../resource/css/printsetup.css'); // external css
$bootstrap = file_get_contents('../resource/css/bootstrap.min.css'); // external css
//$css=$printsetup.$bootstrap;

$mpdf=new Mpdf\Mpdf();
$mpdf->WriteHTML($bootstrap,1);
$mpdf->WriteHTML($html,2);
//$md = UtfString::strcode2utf("&#1575;&#1610;&#1604;&#1575;&#1578; &#1601;&#1610;&#1605;&#1575;&#1575;&#1610;&#1604;&#1575;&#1578; &#1601;&#1610;&#1605;&#1575;");
//$mpdf->SetTitle($md);
// Output a PDF file directly to the browser

if(isset($_GET['dpdf'])=='pdf'){
  ob_clean();
  $mpdf->Output('card.pdf', 'D');
}

//}


if(isset($_GET['sendmail'])) {
  // echo "OK"; die();

  $tmpPassword=$_SESSION['tmp_password'];
  $email=$_GET['email'];
  $emailcontent=<<<EMAIL
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Email Confirmation:</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  /**
   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
   */
  @media screen {
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 400;
      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
    }
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 700;
      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
    }
  }
  /**
   * Avoid browser level font resizing.
   * 1. Windows Mobile
   * 2. iOS / OSX
   */
  body,
  table,
  td,
  a {
    -ms-text-size-adjust: 100%; /* 1 */
    -webkit-text-size-adjust: 100%; /* 2 */
  }
  /**
   * Remove extra space added to tables and cells in Outlook.
   */
  table,
  td {
    mso-table-rspace: 0pt;
    mso-table-lspace: 0pt;
  }
  /**
   * Better fluid images in Internet Explorer.
   */
  img {
    -ms-interpolation-mode: bicubic;
  }
  /**
   * Remove blue links for iOS devices.
   */
  a[x-apple-data-detectors] {
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    color: inherit !important;
    text-decoration: none !important;
  }
  /**
   * Fix centering issues in Android 4.4.
   */
  div[style*="margin: 16px 0;"] {
    margin: 0 !important;
  }
  body {
    width: 100% !important;
    height: 100% !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  /**
   * Collapse table borders to avoid space between cells.
   */
  table {
    border-collapse: collapse !important;
  }
  a {
    color: #1a82e2;
  }
  img {
    height: auto;
    line-height: 100%;
    text-decoration: none;
    border: 0;
    outline: none;
  }
  </style>

</head>
<body style="background-color: #e9ecef;">

  <!-- start preheader -->
  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
   Dear $full_name, Your registration request has been received.
  </div>
  <!-- end preheader -->

  <!-- start body -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="center" valign="top" style="padding: 36px 24px;">
              <a href="http://skygoalsynergy.com" target="_blank" style="display: inline-block;">
                <img src="../resource/img/logo.png" alt="Logo" border="0" width="" >
              </a>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
              <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Thank you for registration !</h1>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p style="margin: 0;">
              Dear $full_name, <br>
              Your registration request has been received.  You loging credetials are as follows:.</p>
              <p>Email Address : $email</p>
              <p>Password : $tmpPassword </p>
            </td>
          </tr>
          <!-- end copy -->

          <!-- start button -->
          <tr>
            <td align="left" bgcolor="#ffffff">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">
                          <a href="skygoalsynergy.com/views/profile.php?email=$email&mobile_no=$mobile&downloadcard=yes" target="_blank" style="display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Download Invitation Card</a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- end button -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p>If you want to pusblish your creative writings on souvenir you are requested to send your writings to : <a style="color: red; font-weight: bold;">info@skygoalsynergy.com </a>  . </p>
              <p style="margin: 0;">We look forward to replying soon . </p>

            </td>
          </tr>
          <!-- end copy -->
          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">Kind Regards, <br> President, The Chittagong University English Alumni Asociation <br> alumni@cuenglishalumni.org <br>
              <a style="text-decoration: none; color: rgb(0, 0, 0);" target="_blank" href="http://skygoalsynyergy.com/">www.skygoalsynyergy.com</a><br>
              <a href="#" style="color: rgb(0, 0, 0); text-decoration: none;">www.facebook.com</a> <br> 880 1714130077 </span><br>
            </td>
          </tr>
          <!-- end copy -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end copy block -->

    <!-- start footer -->
    <tr>
      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start permission -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end permission -->

          <!-- start unsubscribe -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">

            </td>
          </tr>
          <!-- end unsubscribe -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end footer -->

  </table>
  <!-- end body -->
</body>
</html>
EMAIL;

  require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->SMTPDebug  = 0;
  $mail->SMTPAuth   = true;
  $mail->SMTPSecure = "ssl";
  $mail->Host       = "smtp.gmail.com";
  $mail->Port       = 465;
  $mail->AddAddress($email); // To
  $mail->Username="siriusbitm@gmail.com";
  $mail->Password="Team.Sirius";
  $mail->SetFrom('siriusbitm@gmail.com','Skygoal Synergy');
  $mail->addCC('amisalabir@gmail.com','Skygoal Synergy');
  $mail->addBCC('olineitbd@gmail.com','Skygoal Synergy');
  $mail->AddReplyTo("siriusbitm@gmail.com","Skygoal Synergy");
  $mail->Subject = "Skygoal Synergy: Registration Confirmation.";
  $mail->addAttachment("uploads/".$file_name);
  $mail->addAttachment("uploads/".$file_name);
  $mail->addAttachment("uploads/".$file_name);
  $mail->addAttachment("uploads/".$file_name);
  $message =$emailcontent;
  $mail->MsgHTML($message);
  $mail->Send();


  if($mail->Send()){
    unset($tmpPassword,$_SESSION['tmp_password']);
    Message::message("
<div class=\"alert alert-success\">
<strong>Email Sent!</strong> Please check your email for password reset link.
</div>");
  }



}
include('footer.php');
include('footer_script.php');

?>